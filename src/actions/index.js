import axios from 'axios'

export const SHOW_UFS = 'SHOW_UFS'
export const SHOW_DOLAR = 'SHOW_DOLAR'
export const SHOW_TMC = 'SHOW_TMC'

export function showUFs() {

    return (dispatch, getState) => {
        axios.get('https://api.sbif.cl/api-sbifv3/recursos_api/uf/periodo/2008/2019?apikey=7e62e96b1233f1dbe14d9c2395f73b9c2d1d957c&formato=json&callback=despliegue')
            .then((response) => {
                // console.log(response)
                dispatch( { type: SHOW_UFS, payload: response.data.UFs } )
            })
            
    } 
}

export function showTMCs() {
    return (dispatch, getState) => {
        axios.get('https://api.sbif.cl/api-sbifv3/recursos_api/tmc/2019?apikey=10ae29d180d60c4958c99b514523b937a05d1c68&formato=json')
            .then((response) => {
                // console.log(response)
                dispatch( { type: SHOW_TMC, payload: response.data.TMCs } )
            })
            
    } 
}

export function showDolars() {
    return (dispatch, getState) => {
        axios.get('https://api.sbif.cl/api-sbifv3/recursos_api/dolar?apikey=7e62e96b1233f1dbe14d9c2395f73b9c2d1d957c&formato=json')
            .then((response) => {
                // console.log(response)
                dispatch( { type: SHOW_DOLAR, payload: response.data.Dolares } )
            })
            
    } 
}