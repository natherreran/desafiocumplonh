import { SHOW_TMC } from "../actions";

const initialState = {
    list: []
}

export function showTMCs(state = initialState, action) {
    switch (action.type) {
        case SHOW_TMC:
            return Object.assign({}, state, {list: action.payload})
        default:
            return state
    }
}