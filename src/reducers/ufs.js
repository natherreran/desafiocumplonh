import { SHOW_UFS } from "../actions";

const initialState = {
    list: []
}

export function showUFs(state = initialState, action) {
    switch (action.type) {
        case SHOW_UFS:
            return Object.assign({}, state, {list: action.payload})
        default:
            return state
    }
}