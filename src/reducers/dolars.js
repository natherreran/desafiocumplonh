import { SHOW_DOLAR } from "../actions";

const initialState = {
    list: []
}

export function showDolars(state = initialState, action) {
    switch (action.type) {
        case SHOW_DOLAR:
            return Object.assign({}, state, {list: action.payload})
        default:
            return state
    }
}