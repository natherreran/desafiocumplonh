import { combineReducers } from 'redux';
import { showUFs } from './ufs';
import { showDolars } from './dolars';
import { showTMCs } from './tmcs';

const rootReducer = combineReducers({
  uf: showUFs,
  dolar: showDolars,
  tmc: showTMCs,
});

export default rootReducer;

