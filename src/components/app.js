import React, { Component } from 'react';
import { connect } from 'react-redux';
import { showUFs, showDolars, showTMCs } from '../actions';
import Chart from "react-apexcharts";


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      fechaInicio: '',
      fechaTermino: '',
      promedio: 0, 
      minUF: 0,
      maxUF: 0,
      UF: [],
      options: {
        chart: {
          type: 'line'
        },
        xaxis: {
          categories: [],
        }
      },
      series: [{
        name: 'uf',
        data: []
      }],
    }

    this.handlefechaInicioChange = this.handlefechaInicioChange.bind(this);
    this.handlefechaTerminoChange = this.handlefechaTerminoChange.bind(this);
  }

  componentWillMount() {
    this.props.showUFs()
    this.props.showDolars()
    this.props.showTMCs()
  }

  renderDolarsList() {
    return this.props.dolars.map((dolar) => {
      return (
        <p>{dolar.Valor}</p>
      )
    })
    
  }

  renderTMCList() {
    return this.props.tmcs.map((tmc, index) => {
      return (
        <tr>
          <th scope="row">{index}</th>
          <td>{ tmc.Titulo }</td>
          <td>{ tmc.SubTitulo }</td>
          <td>{ tmc.Valor }</td>
          <td>{ tmc.Fecha }</td>
          <td>{ tmc.Tipo }</td>
        </tr>
      )
    })
  }

  handlefechaInicioChange(e) {
    this.setState({fechaInicio: e.target.value})
    console.log(e.target.value);
  }

  handlefechaTerminoChange(e) {
    this.setState({fechaTermino: e.target.value})
    console.log(e.target.value);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.renderUF();
    this.renderPromedioUF();
    this.renderMaxMinUF();
  }

  renderUF() {
    let startDate = this.state.fechaInicio;
    let endDate = this.state.fechaTermino;
    let listUfs = this.props.ufs.filter(uf=> uf.Fecha >= startDate && uf.Fecha <= endDate);
    let arrValor = [];
    let arrFecha = [];
    for (let i=0; i<listUfs.length; i++) {
      arrValor.push(parseFloat(listUfs[i].Valor));
      arrFecha.push(listUfs[i].Fecha);
    }
    this.setState({
      options: {
        xaxis: {
          categories: arrFecha
        },
      },
      series: [
        {
          data: arrValor
        }
      ]
    })

    return listUfs.map((uf, index) => {
      return (
        <li key= { index }> { uf.Fecha }  - { uf.Valor } </li>
      )
    })
  }

  renderPromedioUF() {
    let total = 0;
    let startDate = this.state.fechaInicio;
    let endDate = this.state.fechaTermino;
    let listUfs = this.props.ufs.filter(uf=> uf.Fecha >= startDate && uf.Fecha <= endDate);
    for (let i=0; i<listUfs.length; i++) {
      total +=  parseFloat(listUfs[i].Valor);
    }
    let result = total/listUfs.length;
    
    this.setState({
      promedio: result
    })
     
  }

  renderMaxMinUF() {
    let startDate = this.state.fechaInicio;
    let endDate = this.state.fechaTermino;
    let listUfs = this.props.ufs.filter(uf=> uf.Fecha >= startDate && uf.Fecha <= endDate);
    let arr = [];

    Array.prototype.max = function() {
      return Math.max.apply(null, this);
    };
    Array.prototype.min = function() {
      return Math.min.apply(null, this);
    };

    for (let i=0; i<listUfs.length; i++) {
      arr.push(parseFloat(listUfs[i].Valor));
    }
    this.setState({
      minUF: arr.min(),
      maxUF: arr.max() 
    })
  }
  
  render() {
    return (
      <div>
        
        <form onSubmit={this.handleSubmit.bind(this)}>
        <div class="super-clave1">
        
        <div class="row">
            <div class="center-area">
              <div class="container-fluid">
                
              </div>
            </div>
        </div>
        <div class="info-mensaje"> 
          
        </div>
        </div>
          <div class="box-input">
            <div class="container-fluid">
              <div class="col-md-6">
                <p class="xs-txt title-input">FECHA INICIO</p>
                <input name="fechaInicio" value={ this.state.fechaInicio } onChange={this.handlefechaInicioChange} type="date"></input>
              </div>
              <div class="col-md-6">
                <p class="xs-txt title-input">FECHA TERMINO</p>
                <input name="fechaTermino" value={ this.state.fechaTermino } onChange={this.handlefechaTerminoChange} type="date"></input>
              </div>
            </div>
            <div class="info-mensaje">
              <button class="btn" type="submit">Consultar</button>
            </div>
          </div>
          
          <div class="metricas">
          <div class="col-sm-3">
            <div class="box-input__darker">
              <div class="container-fluid">
                  <h5>Valor Dolar</h5>
                  <div class="value-info"> { this.renderDolarsList() } </div>
              </div>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="box-input">
              <div class="container-fluid">
                <h5>Promedio UF</h5>
                <p class="value-info"> { this.state.promedio } </p>
              </div>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="box-input">
              <div class="container-fluid">
                <h5>Valor Máximo UF</h5>
                <p class="value-info">{ this.state.maxUF }</p>
              </div>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="box-input">
              <div class="container-fluid">
                <h5>Valor Mínimo UF</h5>
                <p class="value-info"> { this.state.minUF } </p>
              </div>
            </div>
          </div>
          </div>
          


          <div class="box-input">
            <div class="container-fluid">
                <h2>Datos UF</h2>
                <Chart
                options={this.state.options}
                series={this.state.series}
                type="bar"
                width="100%"
              />
            </div>
          </div>
          

          <div class="box-input">
            <div class="container-fluid">

              <h2>Datos TMC 2019</h2>

              <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Titulo</th>
                    <th scope="col">SubTitulo</th>
                    <th scope="col">Valor</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Tipo</th>
                  </tr>
                </thead>
                <tbody>
                  { this.renderTMCList() }
                </tbody>
              </table>
            </div>
          </div>
          
        </form>
  
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ufs: state.uf.list,
    dolars: state.dolar.list,
    tmcs: state.tmc.list
  }
}

export default connect(mapStateToProps, { showUFs, showDolars, showTMCs })(App)

