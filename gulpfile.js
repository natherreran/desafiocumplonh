const gulp = require('gulp'),
sass = require('gulp-sass'),
autoprefixer = require('gulp-autoprefixer');
browserSync = require('browser-sync');
uglify = require('gulp-uglify');

gulp.task('sass',() =>
  gulp.src('theme/scss/style.scss')
    .pipe(sass({
      outputStyle: 'expanded'
    })) 
    .pipe(autoprefixer({
      versions: ['last 2 browsers']
    }))
    .pipe(gulp.dest('theme/'))
);

gulp.task('sass-watch',['sass'], browserSync.reload);

gulp.task('default', () => {
  gulp.watch('theme/scss/style.scss', ['sass-watch', 'sass'])
})